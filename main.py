from functions import m_json
from functions import m_pck

path_1 = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
path_2 = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"

m_pck.check_sensors()

metadata_1 = m_json.get_metadata_from_setup(path_1)
#metadata_2 = m_json.get_metadata_from_setup(path_2)

m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets', metadata_1)
#m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets', metadata_2)


m_json.archiv_json('/home/pi/calorimetry_home/datasheets', path_1,'/home/pi/calorimetry_home/archiv/newton_experiment')
#m_json.archiv_json('/home/pi/calorimetry_home/datasheets', path_2 ,'/home/pi/calorimetry_home/archiv/heat_capacity_experiment')

data_1 =m_pck.get_meas_data_calorimetry(metadata_1)
#data_2 =m_pck.get_meas_data_calorimetry(metadata_2)

m_pck.logging_calorimetry(data_1,metadata_1,'/home/pi/calorimetry_home/archiv/newton_experiment','/home/pi/calorimetry_home/datasheets')
#m_pck.logging_calorimetry(data_2,metadata_2,'/home/pi/calorimetry_home/archiv/heat_capacity_experiment','/home/pi/calorimetry_home/datasheets/')


print('Aufnahme Beendet')


